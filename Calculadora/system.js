var resultados = []

function adiciona(){
    var analise = document.querySelector("#nota").value 
    var texto = document.querySelector("#texto")
    
    if (analise > 10 || analise < 0 || isNaN(analise)){
        alert("Nota Inválida, tente novamente!")
    }
    else{
        resultados.push(analise) 
        texto.value += "A nota " + resultados.length + " é " + analise + "\n"
    }
}

function calculo(){
    if (resultados.length != 0){
        var soma = 0
        for (resultado of resultados){
            soma += parseFloat(resultado)
        }
        var mediaTotal = soma / resultados.length
        var p = document.querySelector("#mostrarMedia")
        p.innerHTML = "A média é: " + mediaTotal
    }
    else{
        alert("Adicione um valor para a média.")
    }
}